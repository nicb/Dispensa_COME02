\chapter{Il Suono Digitale\label{app:suono digitale}}

\renewcommand{\imagedir}{\imagebasedir/suono_digitale}

\section{La rappresentazione digitale di una funzione continua\label{sec:funzione continua}}

\subsection{Il campionamento}

Ci\`o che noi definiamo \emph{suono} \`e una variazione \emph{continua} di
pressione dell'aria, captata dalle nostre orecchie e trasmessa al nostro
cervello il quale compie una sofisticata decodifica.

\`E opportuno sottolineare, nel paragrafo precedente l'aggettivo
\emph{continua}, perch\'e questa caratteristica la rende manipolabile con la
matematica elementare delle funzioni. D'altro lato per\`o, la sua continuit\`a
ne complica la gestione attraverso le attuali macchine di calcolo, perch\'e
queste ultime sono legate a rappresentazioni numeriche discrete (i.e. simboli
numerici) e non si adattano bene a fenomeni continui. Questo problema \`e
stato per\`o affrontato molto presto nell'evoluzione delle tecnologie
computazionali odierne in virt\`u del fatto che la stragrande maggioranza dei
fenomeni naturali hanno caratteristiche di continuit\`a.

Esso \`e stato affrontato nella maniera pi\`u logica e ovvia: per
rappresentare digitalmente una funzione continua \`e sufficiente misurarne i
valori a intervalli di tempo regolari.

Questo significa che all'interno di una macchina di calcolo, il suono \`e
rappresentato da una serie di numeri, i quali a loro volta sono le
\emph{misure} raccolte attraverso trasduttori di un qualche tipo (in genere:
microfoni) e collocate nella memoria del calcolatore. Volendo rappresentare
graficamente tale processo, potremmo ottenere qualcosa del genere:

\begin{figure}[hbt]
\begin{center}
   \includegraphics[width=0.8\textwidth]{\imagedir/suono_dentro_computer}
   \caption{Rappresentazione di un suono all'interno di un computer\label{fig:suono digitale}}
\end{center}
\end{figure}

\ldots ossia una pressoch\'e infinita sequenza di numeri che rappresentano,
ciascuno una misura di pressione dell'aria effettuata in un dato momento.
Il processo che regola l'acquisizione di questa sequenza di numeri si chiama
\emph{campionamento}.

\subsection{La frequenza di campionamento}

La quantit\`a di misure necessarie per unit\`a di tempo (generalmente: al
secondo) si chiama \emph{frequenza di campionamento}.

Quante misure sono necessarie per ottenere una rappresentazione adeguata dei
suoni nel tempo?

\`E intuitivo pensare che misure molto ravvicinate nel tempo
daranno una \emph{definizione} pi\`u precisa (quindi: \emph{migliore})
della funzione descritta, ma allo stesso tempo una quantit\`a maggiore di
misure richiede una capacit\`a di stoccaggio nella memoria pi\`u elevato.
Quindi \`e sempre necessario compiere una scelta operativa sulla quantit\`a di
misure da effettuare nell'unit\`a di tempo prescelta (ad es.: in un secondo).
La quantit\`a minima di misure necessarie a rappresentare una fluttuazione
ondulatoria nel tempo \`e stata approfonditamente studiata negli anni '50
(cfr.\cite{shannon1949mathematical}). Il risultato di questi studi pu\`o
riassumersi come segue:

\begin{definition}
   Occorrono almeno \emph{due} misure per rappresentare un'oscillazione,
   quindi ciascuna frequenza potr\`a essere rappresentata da una frequenza di
   campionamento \emph{almeno} doppia della prima.
\end{definition}

Questo risultato \`e intuitivo: dato che un'oscillazione \`e rappresentata da
una parte ``alta'' e da una parte ``bassa'', per poter tener conto
dell'oscillazione stessa \`e opportuno misurarla \emph{almeno} due volte.
Ci\`o che \`e meno intuitivo \`e che la parola \emph{almeno} nella definizione
espressa pi\`u in alto significa che, quando si cerca di rappresentare una
certa frequenza, la frequenza doppia sar\`a sufficiente a dar conto
\emph{esclusivamente} della frequenza e non di altre caratteristiche.

La figura \vref{fig:nyquist limit 0} illustra ci\`o che succede quando la
frequenza di campionamento \`e esattamente doppia della frequenza che
intendiamo rappresentare.

\begin{figure}[htb]
  \begin{center}
     \includegraphics[width=0.4\textwidth]{\imagedir/nyquist_limit_0}
     \caption{Campionamento di un'oscillazione cosinusoidale alla frequenza $2f_0$\label{fig:nyquist limit 0}}
  \end{center}
\end{figure}

La figura \vref{fig:nyquist limit 1} riporta il solo risultato del
campionamento.

\begin{figure}[htb]
  \begin{center}
     \includegraphics[width=0.4\textwidth]{\imagedir/nyquist_limit_1}
     \caption{Misure risultanti dal campionamento di un'oscillazione cosinusoidale alla frequenza $2f_0$\label{fig:nyquist limit 1}}
  \end{center}
\end{figure}

Come si pu\`o constatare, gli unici parametri che possono essere rilevati in
questa condizione limite sono la frequenza e l'ampiezza dell'oscillazione.
Oltretutto, l'ampiezza \`e precisa solo quando le misure sono prelevate sui
massimi e i minimi della funzione misurata, condizione complicata da imporre
in una situazione reale. Se le misure vengono prese in altro punto, si
ottengono risultati assai diversi, come illustrato nella figura
\vref{fig:nyquist limit 2}.

\begin{figure}[htb]
  \begin{center}
     \includegraphics[width=0.4\textwidth]{\imagedir/nyquist_limit_2}
     \caption{Misure risultanti dal campionamento non--coincidente di un'oscillazione cosinusoidale alla frequenza $2f_0$\label{fig:nyquist limit 2}}
  \end{center}
\end{figure}

Cosa succede se si cerca di rappresentare un'oscillazione superiore alla
met\`a della frequenza di campionamento?


