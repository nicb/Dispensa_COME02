\section{La scomposizione di Fourier in tempo breve (STFT)\label{sec: stft}}

La \emph{Short--Time Fourier Transform} (\emph{STFT}), in italiano
scomposizione di Fourier a tempo breve, rappresenta la soluzione
ingegneristica al problema posto dalla pseudo--periodicit\`a dei suoni. I
suoni sono generalmente altamente pseudo--periodici perch\'e le periodicit\`a
rilevate durano generalmente pochi millisecondi; d'altra parte, \`e noto che
fenomeni periodici ``puri'' sono molto molto rari in natura -- se non del
tutto inesistenti. La scomposizione in serie di Fourier rappresenta per\`o
ancora una soluzione analitica plausibile per tali fenomeni, ammettendo una
serie di astrazioni e di approssimazioni e cercando di correggere il pi\`u
possibile gli errori analitici.

% \begin{figure}[ht]
% \begin{center}
%    \includegraphics[height=0.85\textheight]{\whiteboarddir/20211201-fourier}
%    \caption{Lavagna 01/12/2021 STFT\label{fig: lavagna 202112021}}
% \end{center}
% \end{figure}

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=0.85\textwidth]{\imagedir/STFT_schema_Miron_slides}
   \caption{Schema di una \emph{STFT} (tratto dalle slides \emph{MIR\_feature\_extraction} di Marius Mir\'on, MTG Group Universitat Pompeu Fabra)\label{fig:STFT Miron}}
\end{center}
\end{figure}

La Fig.\vref{fig:STFT Miron} sintetizza a grandi linee come funziona una
scomposizione di Fourier a tempo breve. Si tratta sostanzialmente di prendere
delle porzioni molto brevi di suono con finestre che si sovrappongono tra loro
in modo da coprire sostanzialmente ogni momento. L'azione di sovrapposizione
viene chiamata con la terminologia inglese \emph{overlap}. Nella ricostruzione
inversa, tali finestre vengono risintetizzate e sommate le une alle altre e la
combinazione di questi processi \`e denominata \emph{overlap--add}.

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=0.85\textwidth]{\imagedir/STFT_parameters_Miron_slides}
   \caption{I parametri di una \emph{STFT} (tratto dalle slides \emph{MIR\_feature\_extraction} di Marius Mir\'on, MTG Group Universitat Pompeu Fabra)\label{fig:STFT parameters Miron}}
\end{center}
\end{figure}

La Fig.\vref{fig:STFT parameters Miron} illustra i parametri che
caratterizzano una scomposizione di Fourier a tempo breve, e che ne
condizionano fortemente i risultati. Dalla Sez.\vref{sec:compromesso tempo
frequenza} risulta evidente che la dimensione della porzione di segnale presa
in considerazione (la cosiddetta ``finestra'') \`e di primaria importanza;
tuttavia, nella STFT entra in gioco anche un altro parametro: lo scarto in
campioni tra una finestra e quella seguente. Tale scarto \`e denominato
\emph{hopsize} (da \emph{hop} == salto) ed \`e spesso rappresentato in due
modi diversi: una proporzione in campioni della finestra (ad. es: $h_s = w/4$,
dove $w$ \`e la dimensione della finestra e $h_s$ \`e il \emph{hopsize});
oppure in quantit\`a di sovrapposizioni (\emph{overlap}) nella sequenza delle finestre
(nel caso precedente, $o_{lap} = 4$). \`E intuitivo che un \emph{overlap}
maggiore aumenter\`a il numero delle finestre da calcolare, ma la variazione
temporale del segnale verr\`a meglio rappresentata.

\subsection{\emph{Overlap} delle singole finestre\label{sec:olap}}

\`E opportuno verificare che la sovrapposizione delle finestre non crei
modulazioni d'ampiezza del segnale che ne inficierebbero l'analisi.

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/window_olap_add-singled_2}
   \caption{Overlap delle finestre pari a $2$\label{fig:olap-add singled 2}}
\end{center}
\end{figure}

Dalle Figg.\vref{fig:olap-add singled 2} e \vref{fig:olap-add 2} \`e possibile
constatare che, con una finestra di \emph{Von Hann}, una sovrapposizione di
due finestre, sommate, produce una resa unitaria della finestra globale di
tutto il segnale, senza distorsione alcuna (cf.Fig.\vref{fig:olap-add 2}).

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/window_olap_add_2}
   \caption{Risultato complessivo del overlap--add con overlap pari a $2$\label{fig:olap-add 2}}
\end{center}
\end{figure}

Se l'overlap aumenta, si avr\`a una resa amplificata del segnale, come
illustrato nelle Figg.\vref{fig:olap-add singled 4} e \vref{fig:olap-add 4}.

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/window_olap_add-singled_4}
   \caption{Overlap delle finestre pari a $4$\label{fig:olap-add singled 4}}
\end{center}
\end{figure}

Nel caso specifico della finestra di \emph{Von Hann}, considerando che la
finestra stessa attenua di met\`a il segnale, pu\`o risultare opportuna la
sovrapposizione di 4 finestre (che moltiplica il segnale per due, come
constatato nella Fig.\vref{fig:olap-add 4}) - riequilibrando quindi l'ampiezza
del segnale analizzato.

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/window_olap_add_4}
   \caption{Risultato complessivo del overlap--add con overlap pari a $4$\label{fig:olap-add 4}}
\end{center}
\end{figure}

\subsection{Prima versione della STFT\label{sec: stft0}}

Una prima versione della STFT pu\`o quindi essere sintetizzata in queste righe
di pseudo--codice:

\begin{Verbatim}[numbers=left, fontsize=\small, xleftmargin=2mm, frame=single, label={\label{pcode:STFT 0} STFT (pseudocodice)}]
        leggere il segnale
        determinare: larghezza delle finestre e hopsize
        definire un offset mobile dal quale prelevare una finestra di segnale
        dall'inizio sino alla fine del segnale:
          determinare inizio e fine della finestra
          moltiplicare membro a membro il segnale con la finestra prescelta
          eseguire la dft del segnale
          salvare il risultato della DFT in una colonna della matrice di output
          incrementare il offset mobile di hopsize
          ripetere sino alla fine del segnale
\end{Verbatim}

% spiegare il codice

Il codice riportato in \vref{code:STFT 0} realizza questo pseudocodice in
linguaggio \texttt{python}.

\VerbatimInput[numbers=left, fontsize=\tiny, xleftmargin=2mm, frame=single, label={\label{cod:STFT 0} STFT 0}, lastline=27]{\imagedir/STFT_0.py}

In questo codice:
\begin{compactitem}
   \item le righe 4--12 selezionano un segnale, lo misurano e lo normalizzano tra $-1$ e $+1$
   \item le righe 16--17 determinano la larghezza della finestra esterna
           (\texttt{ow}) e di quella interna (\texttt{iw})
   \item la righa 18 determina il \emph{hopsize} (\texttt{hopsize}) come $\frac{finestra_{interna}}{4}$
\end{compactitem}


% disegnare la matrice dell'output
% problema: come fare il display (spiegare waterfall vs. spectrogram}

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/STFT_0}
   \caption{Short--time Fourier transform del segnale, prima versione\label{fig: stft0}}
\end{center}
\end{figure}

\subsection{Ri--combinazione interna della finestra\label{sec: window shuffle}}

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\whiteboarddir/20220201-zero_phase_window_shift}
   \caption{Lo swapping delle finestre\label{fig: window swapping}}
\end{center}
\end{figure}

\subsection{Seconda versione della STFT\label{sec: stft1}}

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/STFT_1}
   \caption{Short--time Fourier transform del segnale, seconda versione\label{fig: stft1}}
\end{center}
\end{figure}
