\needspace{20\baselineskip}
\section{Problemi di ``finestratura'' del segnale\label{sec: finestratura}}

Negli esempi utilizzati sinora abbiamo -- senza esplicitarlo -- utilizzato una
combinazione di frequenze e finestre rettangolari (ossia segmenti di segnale)
multiple tra loro. Ossia: le nostre cosinusoidi iniziavano e finivano,
all'interno della finestra, con un numero di cicli \emph{intero}.

Per es., nella sez.\vref{sec:dft funzione reale composta} abbiamo utilizzato
tre frequenze: $25$, $105$ e $125$ che sono tutte multiple di $5$. La finestra
% utilizzata (\vref[riga n.8]{cod:dft 2}) \`e di $2
% sec$, oss\`ia $4000$ campioni alla frequenza di campionamento $fc=1000.0$ (\vref[riga n.5]{cod:dft 2}).
$4000$ \`e anch'esso un multiplo di $5$ e quindi ciascuna componente compir\`a
un numero intero di cicli all'interno della finestra (nella fattispecie,
le frequenza $25$, $105$ e $125$ si ripeteranno rispettivamente $100$, $410$ e
$500$ volte -- per l'appunto tutti numeri interi).

Anche questa \`e stata una semplificazione per restituire delle immagini
``semplici e pulite'' della scomposizione in serie. Nel mondo reale non
possiamo scegliere noi le frequenze da analizzare ma anzi: eseguiamo le
analisi proprio per capire quali siano le frequenze (e le magnitudini) delle componenti
periodiche semplici presenti all'interno di un segnale periodico composto.

La scelta della grandezza delle finestre d'analisi sar\`a quindi regolata da
altre considerazioni (vedi \vref{sec:compromesso tempo frequenza}).
Cosa succeder\`a quindi quando tali grandezze non saranno pi\`u in rapporti
multipli tra loro? 

Per capirlo, scriviamo un altro programma con una sola componente
cosinusoidale reale non--multipla della larghezza della finestra\footnote%
{%
   incidentalmente, per non riscrivere continuamente la parte di codice che
   esegue la scomposizione di Fourier
   (che rimane comunque sempre uguale) in una funzione denominata \texttt{simple\_dft}
   in un file/libreria separato (\texttt{fourier.py}) il quale
   viene importato all'inizio di ogni file. Questa metodologia viene spiegata
   pi\`u estesamente nella sez.\vref{sec:python} nell'appendice \ref{chap: strumentario}.
}.
Nel codice che segue (cf.\vref{cod:dft3}) utilizzeremo una funzione
cosinusoidale con frequenza $201.52~Hz$ all'interno di una finestra di
$14000$ campioni, al fine di assicurarci che tali numeri non siano multipli tra
loro. Inoltre, al segnale applicheremo una fase casuale (in questo caso
$\frac{\pi}{2.7}$ -- scelta assolutamente arbitraria), per fare in modo che
non si tratti di una cosinusoide o di una sinusoide pura.

\begin{tiny}
\VerbatimInput[numbers=left, xleftmargin=2mm, frame=single, label=dft3.py, lastline=20]{\imagedir/dft3.py}\label{cod:dft3}
\end{tiny}

Questo programma (cf.\vref{cod:dft3}) produce il grafico di Fig.\vref{fig:dft3}.

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/dft3}
   \caption{Scomposizione in serie di Fourier - finestratura rettangolare del segnale\label{fig:dft3}}
\end{center}
\end{figure}

Come si pu\`o notare, questa volta appaiono alcuni problemi:

\begin{compactenum}[a) ]

  \item l'ampiezza del segnale non corrisponde esattamente all'ampiezza che
          abbiamo dato alla funzione periodica semplice in ingresso
  \item la frequenza rilevata non corrisponde \emph{precisamente}
  \item intorno alla frequenza rilevata ne vengono rilevate altre,
        distribuite nelle frequenze adiacenti

\end{compactenum}

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/dft3-boundaries}
   \caption{Il segnale ai bordi della finestra di segnale di Fig.\vref{fig:dft3}\label{fig:dft3 boundaries}}
\end{center}
\end{figure}

Per capire cosa stia succedendo \`e necessario riflettere su ci\`o che accade
\emph{ai bordi} della finestra, come illustrato in Fig.\vref{fig:dft3 boundaries}.
Questa figura \`e una sorta di ingrandimento dell'inizio e della fine della
finestra del segnale illustrato in Fig.\vref{fig:dft3}. Si vedono chiaramente
due cicli incompleti, sia all'inizio che alla fine della finestra. Questi
cicli incompleti, inseriti nella scomposizione in serie di Fourier, non
produrranno i quadrati desiderati ma dei valori spuri. Si tratta quindi a
tutti gli effetti di \emph{errori} della scomposizione legati al fatto che il
segnale non \`e infinito (come richiesto da Fourier) ma \`e invece ristretto a
un insieme di campioni (appunto la \emph{finestra}) delimitato nel tempo.
Riassumendo: se il segnale non \`e infinito, la scomposizione in serie di
Fourier produrr\`a degli errori, oss\`ia un'approssimazione della
scomposizione perfetta.

Come si possono minimizzare questi errori? \`E facile intuire che, minimizzando
il contributo dei bordi della finestra, si possa anche minimizzare l'errore.
Si pu\`o quindi applicare al segnale una finestra non--rettangolare, ad
esempio una finestra dalla forma di una cosinusoide rovesciata, che vada da
$0$ (ai bordi) a $1$ (al centro), e moltiplicare il segnale con essa.

% \begin{figure}[ht]
% \begin{center}
%    \includegraphics[width=\imagewidth]{\whiteboarddir/20211215-windowing}
%    \caption{Lavagna 15/12/2021\label{fig: lavagna 20211215}}
% \end{center}
% \end{figure}

Una finestra del genere si pu\`o produrre con la funzione

\begin{equation}\label{eq: hanning}
     h = -0.5*cos(\frac{2\pi}{l}k)+0.5
\end{equation}

dove:
\begin{compactitem}
   \item $l$ \`e la dimensione della finestra
   \item $k$ \`e la variabile indipendente che va da $0..(l-1)$
\end{compactitem}

Questa funzione si chiama ``finestra di Von Hann'' ma nel gergo ingegneristico
viene spesso chiamata ``finestra di \emph{hanning}'' a causa di un
fraintendimento col nome di un'altro tipo di finestra, chiamata secondo il
nome del suo inventore il matematico americano Richard Hamming.

\needspace{8\baselineskip}
Il codice che segue permette di visualizzare la funzione (calcolata alla linea 7):

\begin{tiny}
\VerbatimInput[numbers=left, xleftmargin=2mm, frame=single, label=hanning\_plot.py, lastline=7]{\imagedir/hanning_plot.py}
\end{tiny}

osservabile in questo grafico:

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/hanning_plot}
   \caption{La finestra di Von Hann (\emph{hanning})\label{fig:hanning plot}}
\end{center}
\end{figure}

\needspace{8\baselineskip}
Per ``finestrare'' una funzione qualsiasi, baster\`a moltiplicare membro a
membro (ossia campione per campione) quella funzione con la funzione di Von Hann.
Questo codice

\begin{tiny}
\VerbatimInput[numbers=left, xleftmargin=2mm, frame=single, label=hanning\_windowing.py, lastline=11]{\imagedir/hanning_windowing.py}
\end{tiny}

esegue la moltiplicazione alla riga 10 e il risultato \`e visibile in Fig.\vref{fig:hanning windowing}.

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/hanning_windowing}
   \caption{Un segnale modulato dalla finestra di Von Hann (\emph{hanning})\label{fig:hanning windowing}}
\end{center}
\end{figure}

Ora \`e possibile osservare come cambia la scomposizione in serie di Fourier
con un segnale moltiplicato per la finestra di Von Hann.
Il codice \vref{cod:dft4} serve per illustrare questa operazione, con la
finestra di Von Hann calcolata nella variabile $h$ alla linea 21
e la moltiplicazione tra il segnale e la finestra alla linea 22.

\begin{tiny}\label{cod:dft4}
  \VerbatimInput[numbers=left, xleftmargin=2mm, frame=single, label={\ref{cod:dft4}: dft4.py}, lastline=25]{\imagedir/dft4.py}
\end{tiny}

\begin{figure}[ht]
\begin{center}
   \includegraphics[width=\imagewidth]{\imagedir/dft4}
   \caption{Scomposizione in serie di Fourier - finestratura del segnale\label{fig:dft4}}
\end{center}
\end{figure}

Dal grafico di Fig.\vref{fig:dft4} \`e possibile constatare
come l'errore ai lati della componente si sia sostanzialmente ridotto.
Anche l'ampiezza risulta dimezzata: ci\`o \`e dovuto al fatto che la finestra
di Von Hann dimezza l'energia complessiva del frammento riducendo l'ampiezza
ai bordi. Per questo sar\`a per\`o sufficente riscalare opportunamente la
magnitudine delle componenti risultanti (per esempio, in questo caso,
moltiplicandole per due).

