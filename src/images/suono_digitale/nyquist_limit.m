clf;
fcn=60;
sincn=1/fcn;
fch=2000;
sinch=1/fch;

dur=0.05;
frq=30;
tn=[-dur-sincn:sincn:dur];
th=[-dur:sinch:dur-sinch];

yn=cos(2*pi*frq*tn);
yh=cos(2*pi*frq*th);

figure(1, "visible", "off")
hold on
stairs(tn, yn, "linewidth", 8);
plot(th, yh, "linewidth", 4);
axis([-dur dur -1.5 1.5])
print -dpng "nyquist_limit_0.png"
hold off

figure(2, "visible", "off")
stairs(tn, yn, "linewidth", 8);
axis([-dur dur -1.5 1.5])
print -dpng "nyquist_limit_1.png"
