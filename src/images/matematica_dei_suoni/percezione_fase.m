fc=48000;
pc=1/fc;

dur=3;    % suoni di tre secondi
t = [0:pc:dur-pc]';

nsamples = dur*fc;
f0 = 400;

y0 = zeros(nsamples, 1);
y1 = zeros(nsamples, 1);

nyquist = fc/2;

nyqidx = floor(nyquist/f0);

for idx=1:2:nyqidx
    frq=idx*f0;
    amp=1/idx;
    rndphase = 2*pi*rand();

    y0 += (amp*sin(2*pi*frq*t));
    y1 += (amp*sin(2*pi*frq*t + rndphase));
end

figure(1, "visible", "off")
plot(t, y0, ";sinusoidi a fase zero;", "linewidth", 6, t, y1, ";sinusoidi a fase random;", "linewidth", 6);
axis([0 0.01 -2.5 2.5])
print -dpng "percezione_fase.png";

audio_rescale=10**(-8/20);
audiowrite("onda_quadra_fase_zero.wav", y0*audio_rescale, fc);
audiowrite("onda_quadra_fase_random.wav", y1*audio_rescale, fc);
