clf;
t = [0:0.001:20];

ysin = sin(t);
ycos = cos(t);

figure(1, "visible", "off")
hold on;
plot(t, ysin, ';sin(t);', "linewidth", 4, t, ycos, ';cos(t);', "linewidth", 4)
axis([t(1) t(end) -1.3 1.3])
hold off;
print -dpng "argomenti_variabili_hifi.png"
