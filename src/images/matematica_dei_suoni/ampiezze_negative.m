%
% Plot che illustra funzioni periodiche di ampiezze diverse
%
fc=500; % frequenza di campionamento
pc=1/fc; % periodo di campionamento

t = [-1:pc:1-pc]; % asse dei tempi (da -10 sec a 10 sec)

f = 1;   % frequenza delle funzioni periodiche (1 Hz)

%
% due ampiezze uguali di segno opposto
%
amp0 = 1.347;
amp1 = -amp0; % la stessa ampiezza amp0, ma di segno opposto (negativa)

%
% le due funzioni
%
w  = 2*pi*f;      % frequenza angolare
alpha = 2*(pi/5); % fase
y0 = amp0*cos(w*t+alpha);
y1 = amp1*cos(w*t+alpha);

figure(1, "visible", "off");
plot(t, y0, ["; amp0 = " num2str(amp0) ";"], "linewidth", 6, ...
     t, y1, ["; amp1 = " num2str(amp1) ";"], "linewidth", 6);
axis([t(1) t(end) amp1-0.05 amp0+0.5])

print -dpng "ampiezze_negative.png"

