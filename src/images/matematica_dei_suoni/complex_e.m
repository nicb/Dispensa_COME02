fc = 1000;
sinc = 1/fc;

dur = 1;
t = [-dur:sinc:dur-sinc];

base0 = 3.2;
base1 = 2.5;
basee = e;

freq = 2;           % freq in Hz
w = freq * 2 * pi;  % freq angolare

yrif = cos(w*t);
ybase0 = base0.**(i*w*t);
ybase1 = base1.**(i*w*t);
ybasee = basee.**(i*w*t);


plot(t, yrif, ';rif;', "linewidth", 4, t, ybase0, '; base 0;', t, ybase1, '; base 1;', t, ybasee, ';base e;')
axis([-dur dur -1.01 1.8])
