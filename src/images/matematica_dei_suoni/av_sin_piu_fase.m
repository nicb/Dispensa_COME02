clf;
%
% Plot che illustra l'identità matematica sin(wt + phi) = cos(phi)sin(wt) + sin(phi)cos(wt) 
%
fc=500; % frequenza di campionamento
pc=1/fc; % periodo di campionamento

t = [-1:pc:1-pc]; % asse dei tempi (da -10 sec a 10 sec)

f = 1;   % frequenza delle funzioni periodiche (1 Hz)

%
% fase diversa da zero
%
alpha = pi/3;

%
% le tre funzioni
%
w  = 2*pi*f;   % frequenza angolare
y0 = sin(w*t+alpha);
y1 = cos(alpha)*sin(w*t);
y2 = sin(alpha)*cos(w*t);

figure(1, "visible", "off");
grid on;
plot(t, y0, ";y(t) = cos(\omega t + \beta);", "linewidth", 6, ...
     t, y1, ";y(t) = cos(\beta)*sin(\omega t);", "linewidth", 6, ...
     t, y2, ";y(t) = sin(\beta)*cos(\omega t);", "linewidth", 6);
axis([t(1) t(end) -1.05 1.5])

print -dpng "av_sin_piu_fase.png"
