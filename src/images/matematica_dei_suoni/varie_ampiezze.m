%
% Plot che illustra funzioni periodiche di ampiezze diverse
%
fc=500; % frequenza di campionamento
pc=1/fc; % periodo di campionamento

t = [-1:pc:1-pc]; % asse dei tempi (da -10 sec a 10 sec)

f = 1;   % frequenza delle funzioni periodiche (1 Hz)

%
% tre ampiezze diverse
%
amp0 = 2.7;
amp1 = 0.23;
amp2 = 0.66;

%
% le tre funzioni
%
w  = 2*pi*f;      % frequenza angolare
alpha = 2*(pi/5); % fase
y0 = amp0*cos(w*t+alpha);
y1 = amp1*cos(w*t+alpha);
y2 = amp2*cos(w*t+alpha);

figure(1, "visible", "off");
plot(t, y0, ["; amp0 = " num2str(amp0) ";"], "linewidth", 6, ...
     t, y1, ["; amp1 = " num2str(amp1) ";"], "linewidth", 6, ...
     t, y2, ["; amp2 = " num2str(amp2) ";"], "linewidth", 6);
axis([t(1) t(end) -amp0-0.05 amp0+1.5])

print -dpng "varie_ampiezze.png"

