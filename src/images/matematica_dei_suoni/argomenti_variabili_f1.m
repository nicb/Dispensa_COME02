clf;
t = [0:0.001:3];

f = 1;

ysin = sin(2*pi*f*t);
ycos = cos(2*pi*f*t);

figure(1, "visible", "off")
hold on;
plot(t, ysin, ';sin(2*\pi*1*t);', "linewidth", 4, t, ycos, ';cos(2*\pi*1*t);', "linewidth", 4)
axis([t(1) t(end) -1.3 1.3])
hold off;
print -dpng "argomenti_variabili_f1.png"
