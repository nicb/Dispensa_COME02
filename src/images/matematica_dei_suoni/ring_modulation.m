%
% Plot che illustra la modulazione ad anello
%
fc=48000; % frequenza di campionamento
pc=1/fc; % periodo di campionamento

t = [-2:pc:2-pc]; % asse dei tempi (da -2 sec a 2 sec)

fph = 200;       % frequenza portante armonica (in Hz)
fmh = 25;
fpi = 199.199;   % frequenza portante (in Hz)
fmi = 23.23;     % frequenza modulante (in Hz)

%
% le tre funzioni
%
wph  = 2*pi*fph;      % frequenza angolare portante
wmh  = 2*pi*fmh;      % frequenza angolare modulante
wpi  = 2*pi*fpi;      % frequenza angolare portante
wmi  = 2*pi*fmi;      % frequenza angolare modulante
alpha = 0;          % fase
yph = cos(wph*t+alpha);
ymh = cos(wmh*t+alpha);
ypi = cos(wpi*t+alpha);
ymi = cos(wmi*t+alpha);
yrmh = yph.*ymh;       % ring modulation!
yrmi = ypi.*ymi;       % ring modulation!

figure(1, "visible", "off");
subplot(2,1,1)
plot(t, ypi, ["; fp = " num2str(fpi) ";"], "linewidth", 6, ...
     t, ymi, ["; fm = " num2str(fmi) ";"], "linewidth", 6, ...
     t, yrmi, ["; rm = " num2str(fpi-fmi) " + " num2str(fpi+fmi) ";"], "linewidth", 6);
axis([-0.03 0.03 -1.05 3.5])
subplot(2,1,2)
plot(t, yrmi, ["; rm = " num2str(fpi-fmi) " + " num2str(fpi+fmi) ";"], "linewidth", 6);
axis([-0.2 0.2 -1.05 1.5])

print -dpng "ring_modulation.png"

audio_rescale=10**(-8/20);
audiowrite("ring_modulation_armonica.wav", yrmh*audio_rescale, fc);
audiowrite("ring_modulation_inarmonica.wav", yrmi*audio_rescale, fc);
