clf;
t = [0:0.001:8];

fs = [ 0.5 1 2 3 ];

ysin = [];
ycos = [];

for f=1:length(fs)
  ysin(f,:) = sin(2*pi*fs(f)*t);
  ycos(f,:) = cos(2*pi*fs(f)*t);
endfor

figure(1, "visible", "off")
subplot(2,2,1)
plot(t, ysin(1,:), ['; f = ' num2str(fs(1)) ';'], "linewidth", 4, t, ycos(1,:), [';f = ' num2str(fs(1)) ';'], "linewidth", 4)
axis([t(1) t(end) -1.1 2.2])
subplot(2,2,2)
plot(t, ysin(2,:), ['; f = ' num2str(fs(2)) ';'], "linewidth", 4, t, ycos(2,:), [';f = ' num2str(fs(2)) ';'], "linewidth", 4)
axis([t(1) t(end) -1.1 2.2])
subplot(2,2,3)
plot(t, ysin(3,:), ['; f = ' num2str(fs(3)) ';'], "linewidth", 4, t, ycos(3,:), [';f = ' num2str(fs(3)) ';'], "linewidth", 4)
axis([t(1) t(end) -1.1 2.2])
subplot(2,2,4)
plot(t, ysin(4,:), ['; f = ' num2str(fs(4)) ';'], "linewidth", 4, t, ycos(4,:), [';f = ' num2str(fs(4)) ';'], "linewidth", 4)
axis([t(1) t(end) -1.1 2.2])
print -dpng "argomenti_variabili_fs.png"
