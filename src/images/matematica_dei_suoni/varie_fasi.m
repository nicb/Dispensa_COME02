%
% Plot che illustra funzioni periodiche che partono da punti diversi
%
fc=500; % frequenza di campionamento
pc=1/fc; % periodo di campionamento

t = [-1:pc:1-pc]; % asse dei tempi (da -10 sec a 10 sec)

f = 1;   % frequenza delle funzioni periodiche (1 Hz)

%
% tre angoli di partenza diversi
%
alpha0 = 0; 
alpha1 = 3*pi/4;
alpha2 = -3*(pi/5);

%
% le tre funzioni
%
w  = 2*pi*f;   % frequenza angolare
y0 = cos(w*t+alpha0);
y1 = cos(w*t+alpha1);
y2 = cos(w*t+alpha2);

figure(1, "visible", "off");
plot(t, y0, ["; alpha0 = " num2str(alpha0) ";"], "linewidth", 6, ...
     t, y1, ["; alpha1 = " num2str(alpha1) ";"], "linewidth", 6, ...
     t, y2, ["; alpha2 = " num2str(alpha2) ";"], "linewidth", 6);
axis([t(1) t(end) -1.05 1.5])

print -dpng "varie_fasi.png"
