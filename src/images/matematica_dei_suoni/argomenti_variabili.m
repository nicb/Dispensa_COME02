clf;
t = [0:20];

ysin = sin(t);
ycos = cos(t);

figure(1, "visible", "off")
hold on;
plot(t, ysin, ';sin(t);', "linewidth", 4, t, ycos, ';cos(t);', "linewidth", 4)
axis([t(1) t(end) -1.3 1.3])
stem(t, ysin)
stem(t, ycos)
hold off;
print -dpng "argomenti_variabili.png"
