#
import numpy as np
import matplotlib.pyplot as plt

dur = 2
nsamples = 1000.0
x = np.arange(-0.5, dur, 1/nsamples)

freq = 5
phase = np.pi/5.0

y = np.cos(2*np.pi*x+phase)

import com_plot_params as cpars

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
plt.stem(x[400:-400], y[400:-400], markerfmt='C3-')
plt.plot(x, y, linewidth=4)
plt.savefig('integrale_sinusoidale.png')
