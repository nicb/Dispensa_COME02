import numpy as np
import matplotlib.pyplot as plt
from fourier import simple_dft

fc=10000.0
nyq=fc/2.0
pc=1/fc
dur = 0.7
t = np.arange(-dur, dur, pc)

freq = 201.52
phase = np.pi/2.7
amp = 0.85
winsize=512
offset=5000

y = amp*np.cos(2*np.pi*freq*t+phase)   # funzione reale

(dftout, F) = simple_dft(y[offset:], fc, winsize)

import com_plot_params as cpars

lw = 0.5
fig0, ax = plt.subplots(2,1, figsize=(cpars.FIGSIZE[0], cpars.FIGSIZE[1]*2), dpi=cpars.FIGDPI)
ax[0].axhline(color='b',lw=lw).set_visible(True)
ax[0].axvline(color='b',lw=lw).set_visible(True)
ax[0].plot(t, y)
ax[0].axis([-0.02, 0.02, -2.1, 2.1])
ax[1].axhline(color='b',lw=lw).set_visible(True)
ax[1].axvline(color='b',lw=lw).set_visible(True)
ax[1].stem(F, np.abs(dftout))
ax[1].axis([-500, 500, -0.1, 0.5])
plt.savefig('dft3.png')

fig1, ax = plt.subplots(1,2, figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
#
# we cheat a bit to show the rectangular (boxcar) window
#
boxcar = np.ones(len(y))
boxcar[0:5] = boxcar[-5:] = 0
y = y*boxcar
ax[0].plot(t, y, t, boxcar, fillstyle='full')
ax[0].axis([-dur*1.001, -dur*0.99, -1.1, 1.1])
ax[0].axhline(color='b',lw=lw).set_visible(True)
ax[1].plot(t, y, t, boxcar, fillstyle='full')
ax[1].axis([dur*0.99, dur*1.001, -1.1, 1.1])
ax[1].axhline(color='b',lw=lw).set_visible(True)
plt.savefig('dft3-boundaries.png')
