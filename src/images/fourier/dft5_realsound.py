import numpy as np
import scipy.io.wavfile as wav
import matplotlib.pyplot as plt

fc, campioni = wav.read("violaovertonescut.wav")
left = campioni[:,0]
dur = len(left)/fc
pc = 1/float(fc)
t = np.arange(0,dur,pc)
nbits = 15
smax = (2**nbits)-1
left = left/smax

from fourier import simple_dft
winsize = 2048
offset = 1*fc
frame = left [offset:offset+winsize]
(dft,f)=simple_dft(frame,fc,winsize)
dft = np.abs(dft)

import com_plot_params as cpars

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
plt.stem(f,dft)
plt.axis([0,3000,-0.001,np.max(dft)*1.1])
plt.savefig("./dft5_realsound.png")
