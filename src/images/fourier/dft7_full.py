import numpy as np
import scipy.io.wavfile as wav
import matplotlib.pyplot as plt

fc, campioni = wav.read("violaovertonescut.wav")
left = campioni[:,0]
dur = len(left)/fc
pc = 1/float(fc)
t = np.arange(0,dur,pc)
nbits = 15
smax = (2**nbits)-1
left = left/smax

from fourier import full_dft, simple_dft
ow = 2044
iw = 511
wdiff = int((ow-iw)/2)
window = np.hanning
offset = 1*fc
frame = left [offset:offset+ow]
(dft0,f0) = full_dft(frame, fc, ow, iw, window)
dft0 = np.abs(dft0)
hanning = np.hanning(iw)
hframe = frame[wdiff:wdiff+iw]*hanning

(dft1,f1) = simple_dft(hframe,fc,iw)
dft1 = np.abs(dft1)
#
# il codice che segue serve per normalizzare entrambe le funzioni,
# per dimostrare che una trasformata zero padded e` effettivamente
# l'interpolazione della trasformata non-zero padded
# 
dft1ref = np.max(dft1)
dft1refidx = np.argmax(dft1)
dft0refidx = dft1refidx*4
dft0ref = dft0[dft0refidx,0]
dft0rescale = dft1ref/dft0ref
dft0 = dft0*dft0rescale

import com_plot_params as cpars

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
plt.stem(f0,dft0)
plt.stem(f1,dft1,"r")
plt.plot(f0,dft0)
plt.axis([800,1600,-0.001,np.max(dft0)*1.2])
plt.savefig("./dft7_full.png")
