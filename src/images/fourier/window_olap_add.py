import numpy as np
import matplotlib.pyplot as plt
import com_plot_params as cpars

fc = 10000
dur = 2.0
left = np.ones(int(dur*fc))  # creiamo una funzione costante di valore 1
pc = 1/float(fc)
t = np.arange(0,dur,pc)

def overlap_windows(olap):
    offset = 0
    ow = 2048
    iw = 511
    hopsize = int(np.floor(iw/olap))
    nframes = int(np.ceil(len(left)/hopsize))
    olap_add_out = np.zeros(len(left))
    singled_out = 6
    singled_out_data = []
    index = 0
    while(offset+ow<len(left)):
        frame = left[offset:offset+ow]
        fdur = len(frame)/float(fc)
        woffset = int((ow-iw)/2)
        iwin = np.hanning(iw)*left[offset:offset+iw]
        owin = np.zeros(ow)
        owin[woffset:woffset+iw] = iwin
        olap_add_out[offset:offset+ow] += owin
        if index < singled_out:
            singled_out_data.append((list(range(offset, offset+ow)), owin))
        offset+=hopsize
        index+=1
    return (olap_add_out, singled_out_data)

(oa_out_2, sod_2) = overlap_windows(2)
(oa_out_4, sod_4) = overlap_windows(4)

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
for x, y in sod_2:
    plt.plot(x, y)
plt.axis([750, 1500, -0.1, 1.1])

plt.savefig("./window_olap_add-singled_2.png")

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
plt.plot(t, oa_out_2)

plt.savefig("./window_olap_add_2.png")

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)


for x, y in sod_4:
    plt.plot(x, y)
plt.axis([750, 1500, -0.1, 1.1])

plt.savefig("./window_olap_add-singled_4.png")

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
plt.plot(t, oa_out_4)

plt.savefig("./window_olap_add_4.png")
