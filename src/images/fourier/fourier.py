import numpy as np

def simple_dft(x, fc, winsize, f_offset=None):
    """
        simple_dft(x, fc, winsize, f_offset=None)

        argomenti:
        x  - il segnale d'ingresso
        fc - la frequenza di campionamento
        winsize - larghezza della finestra
        f_offset - l'offset di calcolo delle frequenze rispetto a -nyquist (default: binsize/2)

    """
    dur = len(x)/float(fc)
    t = np.arange(-dur/2, dur/2, 1/float(fc))
    nyq = fc/2
    binsize = fc/float(winsize)
    if f_offset == None:
        f_offset = binsize/2.0
    F = np.arange(-nyq+f_offset, nyq+f_offset, binsize)# creo un array di frequenze "analitiche" tra -nyquist e +nyquist
    dftout = np.zeros([len(F),1], dtype=np.cdouble)
    #
    # questa e` la dft propriamente detta
    #
    b=0
    for f in F:
        fa = np.exp(-1j*2*np.pi*f*t[0:winsize-1])      # creo la funzione periodica semplice per la f-esima frequenza
        mul = x[0:winsize-1] * fa                      # la moltiplico per la mia funzione in ingresso
        dftout[b,0] = np.sum(mul)/len(mul)             # integro, normalizzo e salvo nella f-esima locazione del vettore di uscita
        b+=1

    return [dftout, F]

def full_dft(x, fc, ow, iw, window):
    """
        full_dft(x, fc, ow, iw, window)

        argomenti:
        x  - il segnale d'ingresso
        fc - la frequenza di campionamento
        ow - la finestra esterna (potenza di 2)
        iw - la finestra interna (< di ow, dispari)
        window - tipo della finestra

    """
    dur = len(x)/float(fc)
    t = np.arange(-dur/2, dur/2, 1/float(fc))
    nyq = fc/2
    offset = int((ow-iw)/2)
    iwin = window(iw)*x[offset:offset+iw]
    owin = np.zeros(ow)
    owin[offset:offset+iw] = iwin
    f_offset = (fc/float(iw))/2.0
    (dft, f) = simple_dft(owin, fc, ow, f_offset)
    return (dft, f)

def dft(x, fc, ow, iw, window):
    """
        dft(x, fc, ow, iw, window)

        argomenti:
        x  - il segnale d'ingresso
        fc - la frequenza di campionamento
        ow - la finestra esterna (potenza di 2)
        iw - la finestra interna (< di ow, dispari)
        window - tipo della finestra

    """
    dur = len(x)/float(fc)
    t = np.arange(-dur/2, dur/2, 1/float(fc))
    nyq = fc/2
    offset = int((ow-iw)/2)
    iwin = window(iw)*x[offset:offset+iw]
    owin = np.zeros(ow)
    owin[offset:offset+iw] = iwin
    half_ow = int(ow/2)
    owinzp = np.zeros(len (owin))
    owinzp[0:half_ow -1] = owin[half_ow +1:]
    owinzp[half_ow + 1:] = owin[0:half_ow -1]
    dftout = np.fft.fft(owinzp)
    binsize = fc/ow
    f = np.arange(0, fc, binsize)
    return (dftout, f)
