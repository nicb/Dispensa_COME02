import numpy as np
import matplotlib.pyplot as plt
from fourier import simple_dft

fc=10000.0
nyq=fc/2.0
pc=1/fc
dur = 2
nsamples = fc*dur
t = np.arange(-dur/2, dur/2, pc)

freq = 200
phase = np.random.random()*2*np.pi-np.pi
amp = 0.85
winsize=512
offset=int(10000-(winsize/2)) # offset in campioni

y = amp*np.cos(2*np.pi*freq*t+phase)   # funzione reale

h = (-0.5*np.cos(((2*np.pi)/winsize)*np.arange(0,winsize)))+0.5 # finestra di Von Hann (hanning)
yh = y[offset:offset+winsize] * h


(dftout, F) = simple_dft(yh, fc, winsize)

import com_plot_params as cpars

lw = 0.5
fig, ax = plt.subplots(2,1,figsize=(cpars.FIGSIZE[0], cpars.FIGSIZE[1]*2),dpi=cpars.FIGDPI)
ax[0].axhline(color='b',lw=lw).set_visible(True)
ax[0].axvline(color='b',lw=lw).set_visible(True)
ax[0].plot(t[offset:offset+winsize], yh)
#ax[0].axis([-0.02, 0.02, -2.1, 2.1])
ax[1].axhline(color='b',lw=lw).set_visible(True)
ax[1].axvline(color='b',lw=lw).set_visible(True)
ax[1].stem(F, np.abs(dftout))
ax[1].axis([-500, 500, -0.1, 0.5])
plt.savefig('dft4.png')
