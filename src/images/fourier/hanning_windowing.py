import numpy as np
import matplotlib.pyplot as plt

winsize = 128           # la dimensione della finestra
k = np.arange(winsize)  # un vettore 0..127

h = -0.5*np.cos((2*np.pi/winsize)*k)+0.5  # la funzione di Von Hann (hanning)
y = np.cos((2*np.pi/winsize)*10*k)        # una funzione periodica qualsiasi

hk = h * y                                # moltiplicazione membro a membro
                                          # dei due vettori

import com_plot_params as cpars

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
plt.plot(k, h, k, hk)
plt.savefig("./hanning_windowing.png")
