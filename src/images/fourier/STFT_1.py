import numpy as np
import scipy.io.wavfile as wav

filename = "violaovertonescut.wav"
fc, campioni = wav.read(filename)
left = campioni[:,0]
dur = len(left)/fc
pc = 1/float(fc)
t = np.arange(0,dur,pc)
nbits = 15
smax = (2**nbits)-1
left = left/smax

from fourier import dft
offset = 0
ow = 2048
iw = 511
hopsize = int(np.floor(iw/4.0))
nframes = int(np.ceil(len(left)/hopsize))
stft = np.zeros([ow, nframes], dtype=np.cdouble)
index = 0
while(offset+ow<len(left)):
    frame = left[offset:offset+ow]
    (dftout, f) = dft(frame, fc, ow, iw, np.hanning)
    stft[:,index] = dftout
    offset+=hopsize
    index+=1

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import com_plot_params as cpars

hoptime = hopsize/fc
tframe = np.arange(0,dur, hoptime)
X, Y = np.meshgrid(tframe, f)
Z = np.abs(stft)

hstretch = 8
fig, ax = plt.subplots(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
im = ax.imshow(Z, interpolation='bilinear',
               cmap=cm.hot, origin='lower', vmax=np.max(Z), vmin=np.min(Z),
               extent=(-0.5,nframes*hstretch-0.5, -0.5, ow-0.5))
frq_list = np.arange(0, fc, 5000)
binsize = fc/float(ow)
bin_list = [int(f/binsize) for f in frq_list]
y_label_list = [(bin_list[idx], frq_list[idx]) for idx in range(len(frq_list))]
ax.set_yticks([l[0] for l in y_label_list])
ax.set_yticklabels([l[1] for l in y_label_list])
x_label_list = [(int(x*hstretch), ("%.1f" % (x*hoptime))) for x in np.arange(0, nframes, nframes/4.0)]
ax.set_xticks([l[0] for l in x_label_list])
ax.set_xticklabels([l[1] for l in x_label_list])
plt.savefig("./STFT_1.png")
