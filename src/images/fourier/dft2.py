import numpy as np
import matplotlib.pyplot as plt

fc=1000.0
nyq=fc/2.0
pc=1/fc
dur = 2
nsamples = fc*dur
t = np.arange(-dur, dur, pc)

freq = [25, 105, 125]
phase = [0, 0, 0]
amp = [0.65, 0.35, 0.3]

y = amp[0]*np.cos(2*np.pi*freq[0]*t+phase[0]) + \
    amp[1]*np.cos(2*np.pi*freq[1]*t+phase[1]) + \
    amp[2]*np.cos(2*np.pi*freq[2]*t+phase[2])      # funzione reale

binsize = 10
F = np.arange(-nyq+(binsize/2), nyq, binsize)         # creo un array di frequenze "analitiche" tra -nyquist e +nyquist
dftout = np.zeros([len(F),1], dtype=np.cdouble)

#
# questa e` la dft propriamente detta
#
b=0
for f in F:
    fa = np.exp(-1j*2*np.pi*f*t)          # creo la funzione periodica semplice per la f-esima frequenza
    mul = y * fa                          # la moltiplico per la mia funzione in ingresso
    dftout[b,0] = np.sum(mul)/len(mul)    # integro, normalizzo e salvo nella f-esima locazione del vettore di uscita
    b+=1

import com_plot_params as cpars

lw = 0.5
fig, ax = plt.subplots(2,1,figsize=(cpars.FIGSIZE[0], cpars.FIGSIZE[1]*2),dpi=cpars.FIGDPI)
ax[0].axhline(color='b',lw=lw).set_visible(True)
ax[0].axvline(color='b',lw=lw).set_visible(True)
ax[0].plot(t, y)
ax[0].axis([-0.2, 0.2, -2.1, 2.1])
ax[1].axhline(color='b',lw=lw).set_visible(True)
ax[1].axvline(color='b',lw=lw).set_visible(True)
ax[1].stem(F, np.abs(dftout))
ax[1].axis([-150, 150, -0.1, 0.4])
plt.savefig('dft2.png')
