import numpy as np

fc=44100.0
nyq=fc/2.0
even_size = 128
even_bin = fc/even_size
odd_size  = even_size-1
odd_bin  = fc/odd_size

even_F = np.arange(-nyq+(even_bin/2.0), nyq, even_bin)
odd_F  = np.arange(-nyq+(odd_bin/2.0), nyq, odd_bin)
even_y = np.hanning(even_size)
odd_y  = np.hanning(odd_size)

import matplotlib.pyplot as plt
import com_plot_params as cpars

plt.rc('axes', titlesize=6, labelsize=6)
plt.rc('xtick', labelsize=6)
plt.rc('ytick', labelsize=6)
lw = 0.5
frange = 2000
fig, ax = plt.subplots(2,1,figsize=(cpars.FIGSIZE[0], cpars.FIGSIZE[1]),dpi=cpars.FIGDPI, sharex=True)
fig.tight_layout()
ax[0].margins(0)
ax[0].axhline(color='b',lw=lw).set_visible(True)
ax[0].axvline(color='b',lw=lw).set_visible(True)
ax[0].stem(even_F, even_y)
ax[0].axis([-frange, frange, -0.1, 1.1])
ax[0].set_title("Finestra pari (dim = %d)" % (even_size))
ax[1].margins(0)
ax[1].axhline(color='b',lw=lw).set_visible(True)
ax[1].axvline(color='b',lw=lw).set_visible(True)
ax[1].stem(odd_F, odd_y)
ax[1].axis([-frange, frange, -0.1, 1.1])
ax[1].set_title("Finestra dispari (dim = %d)" % (odd_size))
plt.savefig('odd_even.png')

fig, ax = plt.subplots(2,1,figsize=(cpars.FIGSIZE[0], cpars.FIGSIZE[1]),dpi=cpars.FIGDPI, sharex=True)
fig.tight_layout()

pltr = (60, 67, 0.995, 1.001)
ax[0].margins(0)
ax[0].axhline(color='b',lw=lw).set_visible(True)
ax[0].axhline(1.0,color='r',lw=lw).set_visible(True)
ax[0].axvline(color='b',lw=lw).set_visible(True)
ax[0].stem(range(pltr[0], pltr[1]), even_y[pltr[0]:pltr[1]], markerfmt='C0+')
ax[0].axis(pltr)
ax[0].set_title("Finestra pari (dim = %d)" % (even_size))
ax[1].margins(0)
ax[1].axhline(1.0,color='r',lw=lw).set_visible(True)
ax[1].axhline(color='b',lw=lw).set_visible(True)
ax[1].axvline(color='b',lw=lw).set_visible(True)
ax[1].stem(range(pltr[0], pltr[1]), odd_y[pltr[0]:pltr[1]], markerfmt='C0+')
ax[1].stem(range(62,65), odd_y[62:65], markerfmt='C1+')
ax[1].axis(pltr)
ax[1].set_title("Finestra dispari (dim = %d)" % (odd_size))
plt.savefig('hanning_one_sample.png')
