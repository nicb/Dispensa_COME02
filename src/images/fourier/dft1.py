import pdb
import numpy as np
import matplotlib.pyplot as plt

fc=1000.0
nyq=fc/2.0
pc=1/fc
dur = 2
nsamples = fc*dur
t = np.arange(-dur, dur, pc)

freq = 105
phase = 0
amp = 0.85

y = amp*np.cos(2*np.pi*freq*t+phase)      # funzione reale

binsize = 10
F = np.arange(-nyq+(binsize/2), nyq, binsize)         # creo un array di frequenze "analitiche" tra -nyquist e +nyquist
dftout = np.zeros([len(F),1], dtype=np.cdouble)

#
# questa e` la dft propriamente detta
#
b=0
for f in F:
    fa = np.exp(-1j*2*np.pi*f*t)          # creo la funzione periodica semplice per la f-esima frequenza
    mul = y * fa                          # la moltiplico per la mia funzione in ingresso
    dftout[b,0] = np.sum(mul)/len(mul)    # integro, normalizzo e salvo nella f-esima locazione del vettore di uscita
    b+=1

import com_plot_params as cpars

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
plt.stem(F, np.abs(dftout))
plt.axis([-nyq, nyq, -0.1, 1])
plt.savefig('dft1.png')
