import numpy as np
import scipy.io.wavfile as wav
import matplotlib.pyplot as plt

fc, campioni = wav.read("violaovertonescut.wav")
left = campioni[:,0]
dur = len(left)/fc
pc = 1/float(fc)
t = np.arange(0,dur,pc)
nbits = 15
smax = (2**nbits)-1
left = left/smax

from fourier import simple_dft
winsize = 2048
offset = 1*fc
frame = left [offset:offset+winsize]
(dft0,f)=simple_dft(frame,fc,winsize)
dft0 = np.abs(dft0)
hanning = -0.5*np.cos(((2*np.pi)/winsize)*np.arange(0,winsize))+0.5
hframe = frame*hanning

(dft1,f) = simple_dft(hframe,fc,winsize)
dft1 = np.abs(dft1)*2

rescale = np.max(dft0)/np.max(dft1)    # riscaliamo i segnali per compararli
dft1 *= rescale

import com_plot_params as cpars

plt.figure(figsize=cpars.FIGSIZE, dpi=cpars.FIGDPI)
plt.plot(f,dft0,linewidth=2, label='finestra rettangolare')
plt.plot(f,dft1, label='finestra di Von Hann')
plt.legend()
plt.axis([0,3000,-0.001,np.max(dft1)*1.1])
plt.savefig("./dft6_windowing.png")
