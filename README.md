# Dispensa per il corso di Composizione Musicale Elettroacustica (COME/02)

Sorgenti `LaTeX` di
una dispensa per il corso di Composizione Musicale Elettroacustica (COME/02).

# LICENZA

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Dispensa_COME02</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/nicb/Dispensa_COME02" rel="dct:source">https://gitlab.com/nicb/Dispensa_COME02</a>.

[Licenza (per esteso)](./LICENSE)
